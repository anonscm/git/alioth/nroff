/*-
 * Copyright (c) 2004, 2005
 *	mirabilos <m@mirbsd.org>
 *
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un-
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided "AS IS" and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person's immediate fault when using the work as intended.
 */

#include <sys/param.h>
#include <stdio.h>
#include <unistd.h>

__RCSID("$MirOS: src/usr.bin/oldroff/nr8pre/nr8pre.c,v 1.5 2020/11/26 01:16:12 tg Exp $");

#define	putstr(x)	fputs((x), stdout)

int
main(int argc, char *argv[])
{
	uint8_t c = 0;
	int i, v = 0, status = 0, max = 127;

	if ((argc > 1) && (argv[1][0] == '-'))
		switch(argv[1][1]) {
		case '7':
			max = 255;
			break;
		}

	setvbuf(stdin, NULL, _IOFBF, 0);
	setvbuf(stdout, NULL, _IOFBF, 0);

	while (read(0, &c, 1) == 1)
		switch (status) {
		case 4:
		case 3:
			if (c == '\'') {
				status = 0;
				c = v;
				goto special;
			}
			if (status == 4)
				break;
			if ((c < '0') || (c > '9')) {
				status = 4;
				break;
			}
			v = (v * 10) + (c - '0');
			break;
		case 2:
			if (c == '\'') {
				status = 3;
				v = 0;
				break;
			}
			status = 0;
			putchar('\\');
			putchar('N');
			goto regular;
			break;
		case 1:
			if (c == 'N') {
				status = 2;
				break;
			}
			status = 0;
			putchar('\\');
			goto regular;
			break;
		case 0:
			if (c == '\\') {
				status = 1;
				break;
			}
		regular:
			if (c < max)
				putchar(c);
			else {
			special:
				putstr("\\(88");
				for (i = 128 | 32768; i > 128; i >>= 1)
					if (c & (i & 255))
						putstr("\\(81");
					else
						putstr("\\(80");
			}
			break;
		}

	return 0;
}
