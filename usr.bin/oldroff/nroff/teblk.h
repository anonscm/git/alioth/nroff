/*-
 * Copyright (c) 1979, 1980, 1981, 1986, 1988, 1990, 1991, 1992
 *     The Regents of the University of California.
 * Copyright (C) Caldera International Inc.  2001-2002.
 * Copyright (c) 2003, 2004, 2023
 *	mirabilos <m@mirbsd.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms,
 * with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * Redistributions of source code and documentation must retain
 * the above copyright notice, this list of conditions and the
 * following disclaimer.  Redistributions in binary form must
 * reproduce the above copyright notice, this list of conditions
 * and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * All advertising materials mentioning features or use of this
 * software must display the following acknowledgement:
 *   This product includes software developed or owned by
 *   Caldera International, Inc.
 *
 * Neither the name of Caldera International, Inc. nor the names
 * of other contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * USE OF THE SOFTWARE PROVIDED FOR UNDER THIS LICENSE BY CALDERA
 * INTERNATIONAL, INC. AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL CALDERA INTERNATIONAL, INC. BE
 * LIABLE FOR ANY DIRECT, INDIRECT INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _TEBLK_H
#define _TEBLK_H

#include "tdef.h"
#ifdef TEBLK_INIT
#include "pathnames.h"

__SCCSID("@(#)ni.c	4.5 (Berkeley) 4/18/91");
__RCSID("$MirOS: src/usr.bin/oldroff/nroff/teblk.h,v 1.3 2023/05/11 16:44:36 tg Exp $");
#endif

#ifndef TEBLK_INIT
extern
#endif
struct {
	/* troff environment block */
	int tabtab[NTAB];
	int ics;
	int ic;
	int icf;
	int chbits;
	int spbits;
	int nmbits;
	int apts;
	int apts1;
	int pts;
	int pts1;
	int font;
	int font1;
	int sps;
	int spacesz;
	int lss;
	int lss1;
	int ls;
	int ls1;
	int ll;
	int ll1;
	int lt;
	int lt1;
	int ad;
	int nms;
	int ndf;
	int fi;
	int cc;
	int c2;
	int ohc;
	int tdelim;
	int hyf;
	int hyoff;
	int un1;
	int tabc;
	int dotc;
	int adsp;
	int adrem;
	int lastl;
	int nel;
	int admod;
	int *wordp;
	int spflg;
	int *linep;
	int *wdend;
	int *wdstart;
	int wne;
	int ne;
	int nc;
	int nb;
	int lnmod;
	int nwd;
	int nn;
	int ni;
	int ul;
	int cu;
	int ce;
	int in;
	int in1;
	int un;
	int wch;
	int pendt;
	int *pendw;
	int pendnf;
	int spread;
	int it;
	int itmac;
	int lnsize;
	int *hyptr[NHYP];
	int line[LNSIZE];
	int word[WDSIZE];
	int blockxxx[EVS - NTAB - 67 - NHYP - LNSIZE - WDSIZE];
} teblk;
#undef TEBLK_INIT

#define ics	teblk.ics
#define ic	teblk.ic
#define icf	teblk.icf
#define chbits	teblk.chbits
#define spbits	teblk.spbits
#define nmbits	teblk.nmbits
#define apts	teblk.apts
#define apts1	teblk.apts1
#define pts	teblk.pts
#define pts1	teblk.pts1
#define font	teblk.font
#define font1	teblk.font1
#define sps	teblk.sps
#define spacesz	teblk.spacesz
#define lss	teblk.lss
#define lss1	teblk.lss1
#define ls	teblk.ls
#define ls1	teblk.ls1
#define ll	teblk.ll
#define ll1	teblk.ll1
#define lt	teblk.lt
#define lt1	teblk.lt1
#define ad	teblk.ad
#define nms	teblk.nms
#define ndf	teblk.ndf
#define fi	teblk.fi
#define cc	teblk.cc
#define c2	teblk.c2
#define ohc	teblk.ohc
#define tdelim	teblk.tdelim
#define hyf	teblk.hyf
#define hyoff	teblk.hyoff
#define un1	teblk.un1
#define tabc	teblk.tabc
#define dotc	teblk.dotc
#define adsp	teblk.adsp
#define adrem	teblk.adrem
#define lastl	teblk.lastl
#define nel	teblk.nel
#define admod	teblk.admod
#define wordp	teblk.wordp
#define spflg	teblk.spflg
#define linep	teblk.linep
#define wdend	teblk.wdend
#define wdstart	teblk.wdstart
#define wne	teblk.wne
#define ne	teblk.ne
#define nc	teblk.nc
#define nb	teblk.nb
#define lnmod	teblk.lnmod
#define nwd	teblk.nwd
#define nn	teblk.nn
#define ni	teblk.ni
#define ul	teblk.ul
#define cu	teblk.cu
#define ce	teblk.ce
#define in	teblk.in
#define in1	teblk.in1
#define un	teblk.un
#define wch	teblk.wch
#define pendt	teblk.pendt
#define pendw	teblk.pendw
#define pendnf	teblk.pendnf
#define spread	teblk.spread
#define it	teblk.it
#define itmac	teblk.itmac
#define lnsize	teblk.lnsize
#define hyptr	teblk.hyptr
#define tabtab	teblk.tabtab
#define line	teblk.line
#define word	teblk.word

struct cta_TEBLK_H {
	char ok[sizeof(teblk) == EVS*sizeof(int) ? 1 : -1];
};

#endif
